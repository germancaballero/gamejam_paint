﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonesWille : MonoBehaviour
{
    GameObject ctrlJuego;
    GameObject botonMemorize;
    GameObject botonInput;

    public void OnClickStart()
    {
        if (ctrlJuego == null)
        {
            ctrlJuego = GameObject.Find("ControladorJuego");
        }
        if (ctrlJuego == null)
        {
            ctrlJuego = GameObject.FindObjectOfType<ControladorJuego>().gameObject;
        }
        ctrlJuego.SetActive(true);
    }
    public void OnClickInput()
    {
        ctrlJuego.GetComponent<ControladorJuego>().ContinuarTurnoPC();
    }
        public void CambiarEstado(bool turnoPC)
    {
        if (!turnoPC)
        {
            botonInput.SetActive(true);
        }
        else
        {
            botonMemorize.SetActive(true);
        }
    }
}
