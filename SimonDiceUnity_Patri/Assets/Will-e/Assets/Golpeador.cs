﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Golpeador : MonoBehaviour
{
    public GameObject camara;
    public GameObject will_e;
    public float duracion = 0.7f;
    public float intervalo = 0.02f;
    public float multiplicador = 70f;
    public Vector2 areaVibracion = new Vector2(0.5f, 0.2f);
    public float aceleracion = 0.1f;
    Animation anim;
    public Vector3 posInicio; 

    private float iniMultiplicador;

    public void Start()
    {
        camara = GameObject.Find("Main Camera");
        if (camara == null)
        {
            camara = GameObject.FindObjectOfType<Camera>().gameObject;
        }
        will_e = this.gameObject;
        iniMultiplicador = multiplicador;
        posInicio = camara.transform.position;
    }
    public void Golpetazo()
    {
        StartCoroutine("MovGolpe");
    }
    IEnumerator MovGolpe()
    {
        for (float f = 0f; f < duracion; f += intervalo)
        {
            camara.transform.localPosition = new Vector3(
                camara.transform.localPosition.x + Mathf.Cos(f * multiplicador) * areaVibracion.x,
                camara.transform.localPosition.y + Mathf.Sin(f * multiplicador) * areaVibracion.y,
                camara.transform.localPosition.z);
            yield return new WaitForSeconds(intervalo);
            multiplicador += aceleracion;
        }
        multiplicador = iniMultiplicador;
        camara.transform.position = posInicio;
    }
}
