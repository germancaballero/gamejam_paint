﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton : MonoBehaviour
{
    public float tiempo = 2f;
    public int idBoton;
    public float intensidadLuz;
    public Light luz;
    public bool Desactivando;
    public bool Desactivado;
    public AudioClip Sonido;
    public ControladorJuego controlador;

    



    // Start is called before the first frame update
    void Start()
    {       
        intensidadLuz = luz.intensity;
        EsperandoIniciar();
    }

    public void EsperandoIniciar() 
    {
        Desactivado = false;
        Desactivando = false;
        luz.intensity = intensidadLuz;
        luz.gameObject.SetActive(false);
    }

   public void ActivarBoton()
    {
        Desactivado = false;
        Desactivando = false;
        luz.intensity = intensidadLuz;
        luz.gameObject.SetActive (true);

        // Hay que llamar al controlador para decirle que hemos hecho clic a este cubo
        if(controlador.turnoUsuario)
        {
            controlador.clickUsuario(idBoton);
        }        

        AudioSource.PlayClipAtPoint(Sonido, Vector3.zero, 1.0f);

        Invoke("desactivarBoton", 0.1f);
    }

    public void desactivarBoton()     
    {
        Desactivando = true;


    }

    // Update is called once per frame
    void Update()
    {

        if (Desactivando && !Desactivado)
        {
            luz.intensity = Mathf.Lerp(luz.intensity, 0, 0.065f);        
        }

        if (luz.intensity <= 0.02) 
        {
            luz.intensity = 0;
            Desactivado = true;
        }
    }

    void OnMouseDown() 
    {
        print(name);
        ActivarBoton();
    }
}
