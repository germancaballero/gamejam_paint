﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public Boton[] botones;
    public List<int> listaAleatoria = new List<int>();
    public bool listaCompleta;
    public bool turnoPc;
    public bool turnoUsuario;
    public float tiempoInicio = 3f;
    public float tiempo = 2f;
    public int contador;
    public int contadorUsuario;
    public int nivelActual;
    public int puntuacionPartida;
    [Range(0.1f, 5f)]
    public float velocidad;

    public Slider sliderVelocidad;
    public Text textoYourScore;
    public Text textoSlider;
    public Text textoGameOver;    
    public Text nivel;
    public Text textoRecord;
    public int numeroBotones = 4;
    public BotonesWille will_e;

    private void Start()
    {
        HacerListaAleatoria();
        turnoPc = true;
        Invoke("TurnoPc", tiempoInicio);
        will_e = GameObject.FindObjectOfType<BotonesWille>();
        textoYourScore.text = "Your score: " + GetScore().ToString();
        textoRecord.text = "Best score: " + GetMaxScore().ToString();
    }

    void HacerListaAleatoria()
    {
        for (int i = 0; i <= 1000; i++) 
        {
            listaAleatoria.Add(Random.Range(0, numeroBotones));
        }
        //listaCompleta = true;
    }

    void TurnoPc() 
    {
        if (/*listaCompleta &&*/ turnoPc)
        {
           botones[listaAleatoria[contador]].ActivarBoton();


            if (contador >= nivelActual)
            {
                nivelActual++;
                CambiarEstado();
            }
            else
            {
                contador++;
            }
            Invoke("TurnoPc", velocidad);
        }
    }

    public void clickUsuario(int idBoton)
    {
        if (idBoton != listaAleatoria[contadorUsuario])
        {
            SaveScore(nivelActual - 1);
            SaveMaxScore(nivelActual - 1);
            GameOver();
            return;
        }
        if (contadorUsuario == contador)
        {
            nivel.text = "Level: " + nivelActual;
            CambiarEstado();
        }
        else 
        {
            contadorUsuario++;
        }
       
    }
    public void CambiarEstado()
    {
        if (turnoPc)
        {
            turnoPc = false;
            turnoUsuario = true;
            will_e.CambiarEstado(false);
            
        }
        else 
        {
            turnoPc = true;
            turnoUsuario = false;
            contador = 0;
            contadorUsuario = 0;
            //TODO: Hahy que activar el boton de clickAnyware el de Input,
            // y mostrar el mensaje
            // Le dice con C# que llame a este método pasado un tiempo. No hay que hacerlo aquí
            Invoke("TurnoPc", tiempo);
        }
    }
    public void ContinuarTurnoPC()
    {
        // Aquí iría el Invoke
    }

    public void GameOver()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("GameOver");
        
    }
   
    public void Reiniciar()
    {
        textoGameOver.gameObject.SetActive(false);
        contador = 0;
        contadorUsuario = 0;
        nivelActual = 0;
        listaAleatoria.Clear();
        HacerListaAleatoria();
        turnoPc = true;
        Invoke("TurnoPc", tiempo);
    }

    public void cambiarVelocidad()
    {
        velocidad = sliderVelocidad.value / 16;
        textoSlider.text = "Velocidad: " + (sliderVelocidad.value / 16).ToString();
    }

    public int GetScore()
    {
       return PlayerPrefs.GetInt("PuntosActuales", 0);
        
    }   
    public void SaveScore(int nivelActual)
    {
        PlayerPrefs.SetInt("PuntosActuales", nivelActual);                      
    }

    public int GetMaxScore()
    {
        return PlayerPrefs.GetInt("PuntosMaximos", 0);
        
    }
    public void SaveMaxScore(int nivelActual)
    {
        if (nivelActual - 1 > GetMaxScore())
        {
            PlayerPrefs.SetInt("PuntosMaximos", nivelActual);
        }
    }
}


