﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControladorEscenas : MonoBehaviour
{

    public AudioSource musica;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.transform.gameObject);
        SceneManager.LoadScene("Menu");
    }

    public void CargaNivel(string escenaJuego) 
    {
        SceneManager.LoadScene (escenaJuego);
    }

    public void CargaDificultad(string nivelDificultad)
    {
        SceneManager.LoadScene(nivelDificultad);

    }

    public void CargaCreditos (string Credits)
    {
        SceneManager.LoadScene (Credits);
    }

    public void CargaMenu(string Menu)
    {
        SceneManager.LoadScene(Menu);
    }

    public void Mute(string Audio)
    {
        AudioListener.pause = !AudioListener.pause;
    }

    public void MuteMusica()
    {
        musica.mute = !musica.mute;
    }

    public void ExitThisGame()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene("Menu");
    }
}
