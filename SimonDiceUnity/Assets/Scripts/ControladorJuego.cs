﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public Boton[] botones;
    public List<int> listaAleatoria = new List<int>();
    public bool listaCompleta;
    public bool turnoPc;
    public bool turnoUsuario;

    public int contador;
    public int contadorUsuario;
    public int nivelActual;
    [Range(0.1f, 2f)]
    public float velocidad;

    public Slider sliderVelocidad;
    public Text textoSlider;
    public Text textoGameOver;    
    public Text nivel;
    public Text textoRecord;

    private void Start()
    {
        HacerListaAleatoria();
        turnoPc = true;
        Invoke("TurnoPc", 1f);
       
    }

    void HacerListaAleatoria()
    {
        for (int i = 0; i <= 1000; i++) 
        {
            listaAleatoria.Add(Random.Range(0, 4));
        }
        listaCompleta = true;
    }

    void TurnoPc() 
    {
        if (listaCompleta && turnoPc)
        {
           botones[listaAleatoria[contador]].ActivarBoton();


            if (contador >= nivelActual)
            {
                nivelActual++;
                CambiarEstado();
            }
            else
            {
                contador++;
            }
            Invoke("TurnoPc", velocidad);
        }
    }

    public void clickUsuario(int idBoton)
    {
        if (idBoton != listaAleatoria[contadorUsuario])
        {
            GameOver();
            return;
        }
        if (contadorUsuario == contador)
        {
            nivel.text = "Nivel: " + nivelActual;
            CambiarEstado();

        }
        else 
        {
            contadorUsuario++;
        }
       
    }

    public void CambiarEstado()
    {
        if (turnoPc)
        {
            turnoPc = false;
            turnoUsuario = true;
        }
        else 
        {
            turnoPc = true;
            turnoUsuario = false;
            contador = 0;
            contadorUsuario = 0;
            Invoke("TurnoPc", 1.2f);
        }

    }

    public void GameOver()
    {
        textoGameOver.gameObject.SetActive(true);
        textoGameOver.text = "Game Over";
        turnoPc = false;
        turnoUsuario = false;
    }
   
    public void Reiniciar()
    {
        textoGameOver.gameObject.SetActive(false);
        contador = 0;
        contadorUsuario = 0;
        nivelActual = 0;
        listaAleatoria.Clear();
        HacerListaAleatoria();
        turnoPc = true;
        Invoke("TurnoPc", 1f);
    }

    public void cambiarVelocidad()
    {
        velocidad = sliderVelocidad.value / 16;
        textoSlider.text = "Velocidad: " + (sliderVelocidad.value / 16).ToString();
    }

    
}

